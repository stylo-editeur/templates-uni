---
abstract:
  - lang: fr
    text: un petit résumé
    text_f: un petit résumé
articleslies:
  - auteur: ''
    titre: ''
    url: ''
authors:
  - forname: Prénom
    surname: Nom
date: 2021/03/05
year: '2021'
month: 'avril'
day: '05'
bibliography: test.bib
director:
  - foaf: ''
    gender: ''
    isni: ''
    viaf: ''
cours:
  - id: FRA3715
    title: Écriture et nouveaux médias
lang: fr
link-citations: true
nocite: '@*'
publisher: 
rights: 
subtitle: sous-titre
subtitle_f: sous-titre
title: Mon titre
title_f: Mon titre
translations:
  - lang: ''
    titre: ''
    url: ''
translator:
  - forname: ''
    orcid: ''
    surname: ''
typeArticle:
  - Essai
---

## Lorem ipsum 

Lorem ipsum dolor[^1] sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

### Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Lorem ipsum

Lorem ipsum dolor sit amet[@posthumus_french_2017], consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Bibliographie 

[^1]: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.