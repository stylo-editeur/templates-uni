---
authors:
  - forname: Margot
    surname: Mellet
    email: "margot.mellet@umontreal.ca"
year: '2021'
session: 'Automne'
bibliography: 'test.bib'
cours:
  - id: FRA3826
    title: Théories de l'édition numérique
lang: fr
link-citations: true
nocite: '@*'
subtitle: sous-titre
subtitle_f: sous-titre
title: Mon titre
title_f: Mon titre
---



## Descriptif

Les environnements numériques remodèlent l'ensemble des processus de production, de circulation et de validation du savoir par le développement de nouveaux outils et pratique de lecture et d'écriture. Au delà d'une transition technologique, le numérique constitue un changement de paradigme culturel : de nos gestes professionnels (éditeur ou traitement de texte) jusqu'à nos espaces de divertissements (de l'ordre de la consommation ou de la culture) en passant par nos modes de communications, nos vies sont structurées par des espaces numériques que nous lisons, enrichissons, et éditons parfois. Parce que justement, les écritures numériques composent une grande partie de nos existences et nos cultures actuelles, il est intéressant de comprendre comment ces dernières fonctionnent : 

Quels sont les processus éditoriaux (production, structuration et diffusion du savoir) qui les soutendent ? 

![image test](media/logo.png)

La question de ce changement culturel sera au coeur de la réflexion du cours consacré à l'étude des formes et enjeux de l'édition numérique.  

Le cours sera structuré en trois axes :
1. Production des contenus : Comment le numérique modifie les pratiques de production de contenus jusqu’à intervenir dans le statut même de ces contenus (rendant par exemple indécise la frontière entre disciplines et entre le statut d'amateur et de spécialiste) ?

2. Circulation des contenus : Comment circulent le savoir ? Comment cohabitent ou non les contenus imprimés et les contenus numériques ? Peut-on identifier des modèles économiques ou humains (le rôle des communauté en ligne par exemple) qui participent à la circulation du savoir ?  

3. Légitimation des contenus : Quels sont les modèles de légitimation des contenus numériques ? Quel rôle joue encore la notion d'auteur ? Quels sont les prochains modèles de légitimation ?

Ces questions seront abordées au travers de cas d’étude permettant d’illustrer les différentes problématiques de l'édition numérique, qui serviront également de support pour une connaissance pratique des outils d’édition.


### Objectifs
L’étude des théories de l’édition numérique est destinée à offrir aux étudiant·e·s des clefs de compréhension et d’appropriation sur les fonctionnements des outils et des espaces numérique qui participent à la structuration du savoir, de la culture et de nos identités. Le cours vise les apprentissages suivants : 
- comprendre les structures et modèles de l’édition numérique ;
- identifier les enjeux sociaux, culturels, politiques de l'édition numérique ;
- acquérir les connaissances théoriques et pratiques d’outils d’écriture, de lecture et d'annotation de contenus numériques ;
- développer sa culture numérique et son regard critique sur les processus d'édition numérique.  

## Calendrier

*Le calendrier est susceptible d’être modifié selon les modalités d’enseignement à l’automne.*

| Séance      |   Date    |   Thème    |
|:-:    |---    |---    |
| 1  | 3 septembre  | Introduction  |
| 2 | 10 septembre  | Qu'est ce que l'édition numérique ? (volet 1) |
| 3 | 17 septembre | Qu'est ce que l'édition numérique ? (volet 2)  |
| 4 | 24 septembre | Web & Hypertexte |
| 5 | 1 octobre | Code & sémantique |
| 6 | 8 octobre | Digital humanities & communauté |
| 7 | 15 octobre | Édition savante & statut de l'éditeur |
| 8 | 29 octobre | Légitimation & question du droit auteur |
| 9 | 5 novembre | Modèles économiques/politiques des formats |
| 10 | 12 novembre | Édition & femmes |
| 11 | 19 novembre | Création littéraire & expérimentation |
| 12 | 26 novembre | Profil & identité |
| 13 | 3 décembre | Clôture |

## Rappel

Selon sa politique sur l'intégrité intellectuelle, l'Université de Montréal s'engage contre le plagiat et la fraude dans les travaux et examens. 

## Bibliographie indicative

*Les œuvres susceptibles de heurter la sensibilité seront signalées au début du cours.*
