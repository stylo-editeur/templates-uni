# Templates-uni

Templates pour export stylo type travaux d'université . 

## Pandoc 

Commandes utilisées pour faire les tests 

### Latex 

#### Commande de base sans toc ni bib 
pandoc --standalone --filter pandoc-citeproc --template=template.tex -f tex -t pdf test.md test.yaml -o Udem.pdf

#### Ref en note de page : 
pandoc --standalone --bibliography test.bib --template=template.latex --citeproc --csl=lettres-et-sciences-humaines-fr.csl -f markdown -t latex test.md -o Udem.tex

#### Ref in texte
pandoc --standalone --bibliography test.bib --template=template.latex --citeproc -f markdown -t latex test.md -o Udem.tex

#### Table of contents 
pandoc --standalone --bibliography test.bib --template=template.latex --toc --citeproc -f markdown -t latex test.md -o Udem.tex

### PDF 

xelatex fichier.tex 

### HTML
